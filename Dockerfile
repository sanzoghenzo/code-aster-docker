ARG OUT_PATH="/opt/aster"
# you can try with gcc10-openblas-ompi4
ARG ASTER_ARCH=gcc9-openblas-ompi4
ARG PREREQ_VERSION=20221225
ARG VERSION=75be6a0e

# BASE STAGE: image to build every other stages on
FROM debian:11 AS compile_aster

# hadolint ignore=DL3008,DL3015
RUN apt-get -qq update \
    && apt-get -qq install -y \
    wget ca-certificates \
    git cmake bison flex tk swig \
    gcc g++ gfortran libopenblas-dev zlib1g-dev libxml2-dev \
    libopenmpi-dev \
    python3-dev python3-scipy cython3 \
    libboost-python-dev libboost-filesystem-dev libboost-regex-dev \
    libboost-system-dev libboost-thread-dev libboost-date-time-dev \
    libboost-chrono-dev libboost-serialization-dev \
    libscalapack-mpi-dev \
    gcc-9 g++-9 gfortran-9 \
    && apt-get -qq clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# TODO: run only when ASTER_ARCH has gcc9 in it
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 30 \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 20 \
    && update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-9 30 \
    && update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-10 20 \
    && update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-10 20 \
    && update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 30 \
    && update-alternatives --install /usr/bin/cpp cpp /usr/bin/cpp-10 20 \
    && update-alternatives --install /usr/bin/cpp cpp /usr/bin/cpp-9 30

# Compilation
#
WORKDIR /root
ARG VERSION
ARG PREREQ_VERSION
ENV PREREQ_BASENAME="codeaster-prerequisites-${PREREQ_VERSION}-oss"
ARG OUT_PATH
ARG ASTER_ARCH
ARG PREREQ_PATH=/root/build
RUN mkdir -p "${PREREQ_PATH}"
RUN wget -q "https://www.code-aster.org/FICHIERS/prerequisites/${PREREQ_BASENAME}.tar.gz" \
    && tar xzf ${PREREQ_BASENAME}.tar.gz \
    && make -C "/root/${PREREQ_BASENAME}" ROOT=unused DEST="${PREREQ_PATH}" ARCH=$ASTER_ARCH

ENV CONFIG_PARAMETERS_addmem=2800
# DEVTOOLS_COMPUTER_ID avoids waf to re-source the environment
ENV DEVTOOLS_COMPUTER_ID=buildkitsandbox
# force parallel build
ENV ENABLE_MPI=1
# custom configuration

# prerequisites paths
ENV LIBPATH_HDF5="${PREREQ_PATH}/hdf5-1.10.9/lib"
ENV INCLUDES_HDF5="${PREREQ_PATH}/hdf5-1.10.9/include"
ENV LD_LIBRARY_PATH="${LIBPATH_HDF5}:${LD_LIBRARY_PATH}"

ENV LIBPATH_MED="${PREREQ_PATH}/med-4.1.1/lib"
ENV INCLUDES_MED="${PREREQ_PATH}/med-4.1.1/include"
ENV PYPATH_MED="${PREREQ_PATH}/med-4.1.1/lib/python3.9/site-packages"
ENV PATH="${PREREQ_PATH}/med-4.1.1/bin:${PATH}"
ENV LD_LIBRARY_PATH="${LIBPATH_MED}:${LD_LIBRARY_PATH}"
ENV PYTHONPATH="${PYPATH_MED}:${PYTHONPATH}"

ENV LIBPATH_METIS="${PREREQ_PATH}/metis-5.1.0_aster4/lib"
ENV INCLUDES_METIS="${PREREQ_PATH}/metis-5.1.0_aster4/include"
ENV LD_LIBRARY_PATH="${LIBPATH_METIS}:${LD_LIBRARY_PATH}"

ENV TFELHOME="${PREREQ_PATH}/mfront-4.1.0"
ENV TFELVERS="4.1.0"
ENV LIBPATH_MFRONT="${PREREQ_PATH}/mfront-4.1.0/lib"
ENV INCLUDES_MFRONT="${PREREQ_PATH}/mfront-4.1.0/include/TFEL-4.1.0"
ENV PATH="${PREREQ_PATH}/mfront-4.1.0/bin:${PATH}"
ENV LD_LIBRARY_PATH="${LIBPATH_MFRONT}:${LD_LIBRARY_PATH}"

ENV LIBPATH_MGIS="${PREREQ_PATH}/mgis-2.1-dev/lib"
ENV INCLUDES_MGIS="${PREREQ_PATH}/mgis-2.1-dev/include"
ENV LD_LIBRARY_PATH="${LIBPATH_MGIS}:${LD_LIBRARY_PATH}"

ENV PATH="${PREREQ_PATH}/homard-11.12_aster2/bin:${PATH}"

ENV LIBPATH_SCOTCH="${PREREQ_PATH}/scotch-7.0.1/lib"
ENV INCLUDES_SCOTCH="${PREREQ_PATH}/scotch-7.0.1/include"
ENV LD_LIBRARY_PATH="${LIBPATH_SCOTCH}:${LD_LIBRARY_PATH}"

ENV LIBPATH_MUMPS="${PREREQ_PATH}/mumps-5.5.1_aster1/lib"
ENV INCLUDES_MUMPS="${PREREQ_PATH}/mumps-5.5.1_aster1/include"
ENV LD_LIBRARY_PATH="${LIBPATH_MUMPS}:${LD_LIBRARY_PATH}"

ENV PATH="${PREREQ_PATH}/miss3d-6.7_aster7/bin:${PATH}"

ENV LIBPATH_MEDCOUPLING="${PREREQ_PATH}/medcoupling-V9_10_0/lib"
ENV INCLUDES_MEDCOUPLING="${PREREQ_PATH}/medcoupling-V9_10_0/include"
ENV PYPATH_MEDCOUPLING="${PREREQ_PATH}/medcoupling-V9_10_0/lib/python3.9/site-packages"
ENV LD_LIBRARY_PATH="${LIBPATH_MEDCOUPLING}:${LD_LIBRARY_PATH}"
ENV PYTHONPATH="${PYPATH_MEDCOUPLING}:${PYTHONPATH}"

ENV PATH="${PREREQ_PATH}/gmsh-4.10.5-Linux64/bin:${PATH}"

ENV PATH="${PREREQ_PATH}/grace-0.0.1/bin:${PATH}"

ENV PYPATH_ASRUN="${PREREQ_PATH}/asrun-2021.0.0-1/lib/python3.9/site-packages"
ENV PATH="${PREREQ_PATH}/asrun-2021.0.0-1/bin:${PATH}"
ENV PYTHONPATH="${PYPATH_ASRUN}:${PYTHONPATH}"

ENV PYPATH_MPI4PY="${PREREQ_PATH}/mpi4py-3.1.3/lib/python3.9/site-packages"
ENV PYTHONPATH="${PYPATH_MPI4PY}:${PYTHONPATH}"

ENV LIBPATH_PARMETIS="${PREREQ_PATH}/parmetis-4.0.3_aster3/lib"
ENV INCLUDES_PARMETIS="${PREREQ_PATH}/parmetis-4.0.3_aster3/include"
ENV LD_LIBRARY_PATH="${LIBPATH_PARMETIS}:${LD_LIBRARY_PATH}"

ENV LIBPATH_MATH="${PREREQ_PATH}/scalapack-2.1.0/lib"
ENV LD_LIBRARY_PATH="${LIBPATH_MATH}:${LD_LIBRARY_PATH}"

ENV LIBPATH_PETSC="${PREREQ_PATH}/petsc-3.17.1_aster/lib"
ENV INCLUDES_PETSC="${PREREQ_PATH}/petsc-3.17.1_aster/include ${PREREQ_PATH}/petsc-3.17.1_aster/lib/petsc4py/include"
ENV PYPATH_PETSC="${PREREQ_PATH}/petsc-3.17.1_aster/lib"
ENV LD_LIBRARY_PATH="${LIBPATH_PETSC}:${LD_LIBRARY_PATH}"
ENV PYTHONPATH="${PYPATH_PETSC}:${PYTHONPATH}"

ENV LIB_BOOST="boost_python3"
ENV INCLUDES_BOOST="/usr/include"

ENV LINKFLAGS="${LINKFLAGS} -Wl,--no-as-needed"

RUN git clone -n https://gitlab.com/codeaster/src.git \
    && git -C src checkout "${VERSION}"
WORKDIR /root/src
RUN ./waf configure --prefix="${PREREQ_PATH}/aster"
RUN ./waf install -j "$(nproc)"

FROM debian:11 as runtime

ARG OUT_PATH
ARG PREREQ_PATH=/root/build

# prerequisites paths
ENV LIBPATH_HDF5="${OUT_PATH}/hdf5-1.10.9/lib"
ENV INCLUDES_HDF5="${OUT_PATH}/hdf5-1.10.9/include"
ENV LD_LIBRARY_PATH="${LIBPATH_HDF5}:${LD_LIBRARY_PATH}"

ENV LIBPATH_MED="${OUT_PATH}/med-4.1.1/lib"
ENV INCLUDES_MED="${OUT_PATH}/med-4.1.1/include"
ENV PYPATH_MED="${OUT_PATH}/med-4.1.1/lib/python3.9/site-packages"
ENV PATH="${OUT_PATH}/med-4.1.1/bin:${PATH}"
ENV LD_LIBRARY_PATH="${LIBPATH_MED}:${LD_LIBRARY_PATH}"
ENV PYTHONPATH="${PYPATH_MED}:${PYTHONPATH}"

ENV LIBPATH_METIS="${OUT_PATH}/metis-5.1.0_aster4/lib"
ENV INCLUDES_METIS="${OUT_PATH}/metis-5.1.0_aster4/include"
ENV LD_LIBRARY_PATH="${LIBPATH_METIS}:${LD_LIBRARY_PATH}"

ENV TFELHOME="${OUT_PATH}/mfront-4.1.0"
ENV TFELVERS="4.1.0"
ENV LIBPATH_MFRONT="${OUT_PATH}/mfront-4.1.0/lib"
ENV INCLUDES_MFRONT="${OUT_PATH}/mfront-4.1.0/include/TFEL-4.1.0"
ENV PATH="${OUT_PATH}/mfront-4.1.0/bin:${PATH}"
ENV LD_LIBRARY_PATH="${LIBPATH_MFRONT}:${LD_LIBRARY_PATH}"

ENV LIBPATH_MGIS="${OUT_PATH}/mgis-2.1-dev/lib"
ENV INCLUDES_MGIS="${OUT_PATH}/mgis-2.1-dev/include"
ENV LD_LIBRARY_PATH="${LIBPATH_MGIS}:${LD_LIBRARY_PATH}"

ENV PATH="${OUT_PATH}/homard-11.12_aster2/bin:${PATH}"

ENV LIBPATH_SCOTCH="${OUT_PATH}/scotch-7.0.1/lib"
ENV INCLUDES_SCOTCH="${OUT_PATH}/scotch-7.0.1/include"
ENV LD_LIBRARY_PATH="${LIBPATH_SCOTCH}:${LD_LIBRARY_PATH}"

ENV LIBPATH_MUMPS="${OUT_PATH}/mumps-5.5.1_aster1/lib"
ENV INCLUDES_MUMPS="${OUT_PATH}/mumps-5.5.1_aster1/include"
ENV LD_LIBRARY_PATH="${LIBPATH_MUMPS}:${LD_LIBRARY_PATH}"

ENV PATH="${OUT_PATH}/miss3d-6.7_aster7/bin:${PATH}"

ENV LIBPATH_MEDCOUPLING="${OUT_PATH}/medcoupling-V9_10_0/lib"
ENV INCLUDES_MEDCOUPLING="${OUT_PATH}/medcoupling-V9_10_0/include"
ENV PYPATH_MEDCOUPLING="${OUT_PATH}/medcoupling-V9_10_0/lib/python3.9/site-packages"
ENV LD_LIBRARY_PATH="${LIBPATH_MEDCOUPLING}:${LD_LIBRARY_PATH}"
ENV PYTHONPATH="${PYPATH_MEDCOUPLING}:${PYTHONPATH}"

ENV PATH="${OUT_PATH}/gmsh-4.10.5-Linux64/bin:${PATH}"

ENV PATH="${OUT_PATH}/grace-0.0.1/bin:${PATH}"

ENV PYPATH_ASRUN="${OUT_PATH}/asrun-2021.0.0-1/lib/python3.9/site-packages"
ENV PATH="${OUT_PATH}/asrun-2021.0.0-1/bin:${PATH}"
ENV PYTHONPATH="${PYPATH_ASRUN}:${PYTHONPATH}"

ENV PYPATH_MPI4PY="${OUT_PATH}/mpi4py-3.1.3/lib/python3.9/site-packages"
ENV PYTHONPATH="${PYPATH_MPI4PY}:${PYTHONPATH}"

ENV LIBPATH_PARMETIS="${OUT_PATH}/parmetis-4.0.3_aster3/lib"
ENV INCLUDES_PARMETIS="${OUT_PATH}/parmetis-4.0.3_aster3/include"
ENV LD_LIBRARY_PATH="${LIBPATH_PARMETIS}:${LD_LIBRARY_PATH}"

ENV LIBPATH_MATH="${OUT_PATH}/scalapack-2.1.0/lib"
ENV LD_LIBRARY_PATH="${LIBPATH_MATH}:${LD_LIBRARY_PATH}"

ENV LIBPATH_PETSC="${OUT_PATH}/petsc-3.17.1_aster/lib"
ENV INCLUDES_PETSC="${OUT_PATH}/petsc-3.17.1_aster/include ${OUT_PATH}/petsc-3.17.1_aster/lib/petsc4py/include"
ENV PYPATH_PETSC="${OUT_PATH}/petsc-3.17.1_aster/lib"
ENV LD_LIBRARY_PATH="${LIBPATH_PETSC}:${LD_LIBRARY_PATH}"
ENV PYTHONPATH="${PYPATH_PETSC}:${PYTHONPATH}"

ENV LIB_BOOST="boost_python3"
ENV INCLUDES_BOOST="/usr/include"

ENV LINKFLAGS="${LINKFLAGS} -Wl,--no-as-needed"

# hadolint ignore=DL3008
RUN apt-get -qq update \
    && apt-get -qq install -y --no-install-recommends \
    python3 libpython3.9 python3-numpy gmsh libopenblas0-openmp openmpi-bin \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY --from=compile_aster --chown=1000:1000 "${PREREQ_PATH}" "${OUT_PATH}"

ENV PATH="${OUT_PATH}/aster/bin:${PATH}"
ARG USER_ID=1000
ARG USER_DIR=/data
RUN mkdir -p ${USER_DIR} && chown ${USER_ID} ${USER_DIR}
WORKDIR ${USER_DIR}
USER 1000
