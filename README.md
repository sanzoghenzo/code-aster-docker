# Code_Aster docker image

Code aster Dockerfile

## Configure

You can set the following environment variables:

- `VERSION`: code aster git branch, tag or commit hash. Defaults to `75be6a0e`
- `ASTER_ARCH`: target architecture. Defaults to `gcc9-openblas-ompi4`
- `PREREQ_VERSION`: Prerequisites package version. Defaults to `20221225`
- `DOCKER_IMAGE`: Name of the docker image to create. `sanzoghenzo/code-aster`

## checks

Run `task lint` to analyze the Dockerfile and ensure it follows the best practices.

## Build

Use `task build` to generate the image.

The `latest` tag will be applied along with the `VERSION` tag.

## Push

Use `task push` to updolad the image to the registry.

NOTE: you need to run `docker login` to provide your credentials first.
